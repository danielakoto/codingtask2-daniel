package edu.towson.cosc435.valis.labsapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Row
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import edu.towson.cosc435.valis.labsapp.ui.theme.LabsAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LabsAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    CodingTask2()
                }
            }
        }
    }
}

@Composable
fun CodingTask2() {
    var count by remember { mutableStateOf(0)}
    // Complete task here
    // Your app must contain a button that when clicked, it increments a counter.
    Button(
        onClick = {
            count++
            if(count == 5){
                count = 0
            }
        }
    ){}
    // Display the current count.
    Row() {
        Text("" + count)
    }
    // When the value of the counter reaches 5, reset the counter.
    // When the value of the counter is odd, color the text Red, when even, color the text Black.
    // Rotating the device should not reset the current counter value.
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LabsAppTheme {
        CodingTask2()
    }
}